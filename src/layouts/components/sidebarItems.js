/*=========================================================================================
Strucutre:
  header      => header menu (does not has any other attribute, just header)
  url         => router path
  name        => name to display in sidebar
  slug        => router path name
  icon        => Feather Icon component/icon name
  tag         => text to display on badge
  tagColor    => class to apply on badge element
  i18n        => Internationalization
  submenu     => submenu of current item (current item will become dropdown)
  isDisabled  => disable sidebar item/group
==========================================================================================*/

export default [
  {
    url: "/home",
    name: "Home",
    slug: "home",
    icon: "HomeIcon"
  },
  {
    url: "/app/db/employee",
    name: "Data Pegawai",
    slug: "database-employee",
    icon: "UsersIcon"
  },
  {
    url: "/app/information-panel",
    name: "Panel Informasi",
    slug: "information-panel",
    icon: "SettingsIcon"
  },
  {
    header: "Apps"
  },
  {
    url: "/profile",
    name: "Profil",
    slug: "profile",
    icon: "UserIcon"
  },
  // {
  //   url: "/app/promotion",
  //   name: "Promosi",
  //   slug: "promotion",
  //   icon: "AwardIcon"
  // },
  // {
  //   url: "/app/retirement",
  //   name: "Pensiun",
  //   slug: "retirement",
  //   icon: "FeatherIcon"
  // },
  {
    url: "/app/report",
    name: "Laporan",
    slug: "report",
    icon: "FileTextIcon"
  }
];
