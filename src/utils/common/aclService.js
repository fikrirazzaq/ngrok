// =============== LIBRARY
import Vue from "vue";
import { AclInstaller, AclCreate, AclRule } from "vue-acl";
import router from "@/router";

// =============== CONFIGURATIONS
const ID_USER_ROLE_KEY = "user_role";

export const getRole = () => {
  return localStorage.getItem(ID_USER_ROLE_KEY);
};

export const setRole = role => {
  localStorage.setItem(ID_USER_ROLE_KEY, role);
};

export const destroyRole = () => {
  localStorage.removeItem(ID_USER_ROLE_KEY);
};

Vue.use(AclInstaller);

let initialRole = getRole() ? getRole() : "public";

export default new AclCreate({
  initial: initialRole,
  notfound: "/not-authorized",
  router,
  acceptLocalRules: true,
  globalRules: {
    admin: new AclRule("admin").generate(),
    employee: new AclRule("employee").or("admin").generate(),
    public: new AclRule("public")
      .or("employee")
      .or("admin")
      .generate()
  }
});
