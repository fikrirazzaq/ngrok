/*=========================================================================================
Object Strucutre:
path                      => router path
name                      => router name
component(lazy loading)   => component to load
meta : {
  rule            => which user can have access (ACL)
  breadcrumb      => Add breadcrumb to specific page
  pageTitle       => Display title besides breadcrumb
}
==========================================================================================*/

import Vue from "vue";
import Router from "vue-router";
import AuthService from "@/utils/common/authService";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      // ==================== MAIN PAGE LAYOUT
      path: "",
      component: () => import("@/layouts/LayoutMain"),
      children: [
        {
          path: "/",
          redirect: "/home"
        },
        {
          path: "/home",
          name: "home",
          component: () => import("@/views/app/Home"),
          meta: {
            rule: "employee"
          }
        },
        {
          path: "/profile",
          name: "profile",
          component: () => import("@/views/app/Profile"),
          meta: {
            rule: "employee"
          }
        },
        {
          path: "/profile/upload/avatar",
          name: "profile-upload-avatar",
          component: () => import("@/views/app/ProfileAvatar"),
          meta: {
            rule: "employee"
          }
        },
        {
          path: "/app/information-panel",
          name: "information-panel",
          component: () => import("@/views/app/InformationPanel"),
          meta: {
            rule: "admin"
          }
        },
        {
          path: "/app/db/employee",
          name: "database-employee",
          component: () => import("@/views/app/DatabaseEmployee"),
          meta: {
            rule: "admin",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Database" },
              { title: "Data Pegawai", active: true }
            ]
          }
        },
        {
          path: "/app/db/employee/profile/add",
          name: "database-employee-profile-add",
          component: () => import("@/views/app/EmployeeAdd"),
          meta: {
            rule: "admin",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Database" },
              { title: "Data Pegawai", url: "/app/db/employee" },
              { title: "Tambah Data", active: true }
            ]
          }
        },
        {
          path: "/app/db/employee/profile/edit",
          name: "database-employee-profile-edit",
          component: () => import("@/views/app/EmployeeEdit"),
          meta: {
            rule: "admin",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Database" },
              { title: "Data Pegawai", url: "/app/db/employee" },
              { title: "Edit Data", active: true }
            ]
          }
        },
        {
          path: "/app/db/employee/profile",
          name: "database-employee-profile",
          component: () => import("@/views/app/Employee"),
          meta: {
            rule: "admin",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Database" },
              { title: "Data Pegawai", url: "/app/db/employee" },
              { title: "Profile Pegawai", active: true }
            ]
          }
        },
        {
          path: "/app/notification",
          name: "notification",
          component: () => import("@/views/app/Notification"),
          meta: {
            rule: "employee",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Panel" },
              { title: "Notifikasi", active: true }
            ]
          }
        },
        {
          path: "/app/promotion",
          name: "promotion",
          component: () => import("@/views/app/Promotion"),
          meta: {
            rule: "admin",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Database" },
              { title: "Data Kenaikan Jabatan", active: true }
            ]
          }
        },
        {
          path: "/app/promotion/employee",
          name: "promotion-employee",
          component: () => import("@/views/app/Employee"),
          meta: {
            rule: "admin",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Database" },
              { title: "Data Kenaikan Jabatan", url: "/app/promotion" },
              { title: "Profile Pegawai", active: true }
            ]
          }
        },
        {
          path: "/app/retirement",
          name: "retirement",
          component: () => import("@/views/app/Retirement"),
          meta: {
            rule: "admin",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Database" },
              { title: "Data Pensiun", active: true }
            ]
          }
        },
        {
          path: "/app/retirement/employee",
          name: "retirement-employee",
          component: () => import("@/views/app/Employee"),
          meta: {
            rule: "admin",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Database" },
              { title: "Data Pensiun", url: "/app/retirement" },
              { title: "Profile Pegawai", active: true }
            ]
          }
        },
        {
          path: "/app/calendar",
          name: "calendar",
          component: () => import("@/views/app/Calendar"),
          meta: {
            rule: "employee",
            breadcrumb: [
              { title: "Home", url: "/" },
              { title: "Apps" },
              { title: "Kalender", active: true }
            ]
          }
        },
        {
          path: "/app/report",
          name: "report",
          component: () => import("@/views/app/Report"),
          meta: {
            rule: "admin"
          }
        }
      ]
    },
    {
      // ==================== BOARD LAYOUT
      path: "",
      component: () => import("@/layouts/LayoutBoard"),
      children: [
        {
          path: "/information-board",
          name: "information-board",
          component: () => import("@/views/app/InformationBoard"),
          meta: {
            rule: "public"
          }
        }
      ]
    },
    {
      // ==================== FULL PAGE LAYOUT
      path: "",
      component: () => import("@/layouts/LayoutFull"),
      children: [
        {
          path: "/login",
          name: "login",
          component: () => import("@/views/full/Login"),
          meta: {
            rule: "public"
          }
        },
        {
          path: "/login/forgot-password",
          name: "forgot-password",
          component: () => import("@/views/full/ForgotPassword"),
          meta: {
            rule: "public"
          }
        },
        {
          path: "/login/register",
          name: "register",
          component: () => import("@/views/full/Register"),
          meta: {
            rule: "public"
          }
        },
        {
          path: "/login/password-setting/verify",
          name: "password-setting",
          component: () => import("@/views/full/PasswordSetting"),
          meta: {
            rule: "public"
          }
        },
        {
          path: "/error-404",
          name: "error-404",
          component: () => import("@/views/full/Error404"),
          meta: {
            rule: "public"
          }
        },
        {
          path: "/not-authorized",
          name: "not-authorized",
          component: () => import("@/views/full/NotAuthorized"),
          meta: {
            rule: "public"
          }
        },
        {
          path: "/maintenance",
          name: "maintenance",
          component: () => import("@/views/full/Maintenance"),
          meta: {
            rule: "public"
          }
        }
      ]
    },
    {
      // ==================== ERROR PAGE
      path: "*",
      redirect: "error-404"
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (
    to.path === "/information-board" ||
    to.path === "/login" ||
    to.path === "/login/forgot-password" ||
    to.path === "/login/register" ||
    to.path === "/login/password-setting/verify" ||
    to.path === "/error-404" ||
    to.path === "/not-authorized" ||
    to.path === "/maintenance" ||
    AuthService.isUserLoggedIn()
  ) {
    return next();
  } else {
    router.push({ name: "login", query: { to: to.path } });
  }
});

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById("loading-bg");
  if (appLoading) {
    appLoading.style.display = "none";
  }
});

export default router;
