function format_date(value, full = true) {
  const monthNames = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
  ];

  const dayNames = [
    "Minggu",
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jumat",
    "Sabtu"
  ];

  let date = new Date(value);

  if (full) {
    return (
      dayNames[date.getDay()] +
      ", " +
      date.getDate() +
      " " +
      monthNames[date.getMonth()] +
      " " +
      date.getFullYear()
    );
  } else {
    return (
      date.getDate() +
      " " +
      monthNames[date.getMonth()] +
      " " +
      date.getFullYear()
    );
  }
}

function getMarital(status, num) {
  if (num == 0) {
    return status;
  } else {
    return status + ", " + num + " anak";
  }
}

export const fields = {
  "Unit Kerja": "unit",
  "Sub Unit Kerja": "sub_unit",
  "Identitas Pegawai": {
    callback: value => {
      return (
        "NIP:\t" +
        value.nip +
        "\n" +
        "Nama Lengkap:\t" +
        value.name +
        "\n" +
        "TTL:\t" +
        value.birth_place +
        "," +
        format_date(value.birth_date, false) +
        "\n" +
        "Jenis Kelamin:\t" +
        value.gender +
        "\n" +
        "Agama:\t" +
        value.religion +
        "\n" +
        "Status Kawin:\t" +
        getMarital(value.marital, value.num_of_child) +
        "\n" +
        "Golongan Darah:\t" +
        value.blood_type +
        "\n" +
        "Alamat:\t" +
        value.address +
        "\n" +
        "Email:\t" +
        value.email +
        "\n" +
        "Status Kepegawaian:\t" +
        value.position_type +
        "\n" +
        "Jumlah Cuti:\t" +
        value.cuti +
        "\n" +
        "Tanggal Kenaikan Jabatan:\t" +
        format_date(value.date_next_promotion) +
        "\n" +
        "Tanggal Pensiun:\t" +
        format_date(value.date_retire) +
        "\n"
      );
    }
  },
  Jabatan: {
    field: "jabatan",
    callback: value => {
      return (
        value.nama + "\n" + value.keputusan + "\n" + format_date(value.tanggal)
      );
    }
  },
  "Pangkat / Golongan": {
    field: "golongan",
    callback: value => {
      return (
        value.nama + "\n" + value.ruang + "\n" + format_date(value.tanggal)
      );
    }
  },
  Pendidikan: {
    field: "education",
    callback: value => {
      let edu = "";
      if (value.length != 0) {
        for (let index = 0; index < value.length; index++) {
          edu +=
            value[index].degree +
            " " +
            value[index].major +
            "\n" +
            value[index].year +
            " " +
            value[index].institution +
            "\n\n";
        }
        return edu + "\n";
      } else {
        return edu;
      }
    }
  },
  Diklat: {
    field: "diklat",
    callback: value => {
      let edu = "";
      if (value.length != 0) {
        for (let index = 0; index < value.length; index++) {
          edu += value[index].year + " " + value[index].name + "\n\n";
        }
        return edu + "\n";
      } else {
        return edu;
      }
    }
  }
};
