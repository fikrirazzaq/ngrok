// =============== LIBRARY
import "prismjs";
import "@/utils/filters";
import "@/registerServiceWorker";
import Vue from "vue";
import Vuesax from "vuesax";
import VeeValidate, { Validator } from "vee-validate";
import { VueHammer } from "vue2-hammer";
import vSelect from "vue-select";
import App from "@/App";
import router from "@/router";
import store from "@/store";
import acl from "@/utils/common/aclService";
import ApiService from "@/utils/common/apiService";
import { AUTH_IS_AUTHORIZED } from "./store/action-types";

// =============== CSS
import "material-icons/iconfont/material-icons.css";
import "prismjs/themes/prism-tomorrow.css";
import "vuesax/dist/vuesax.css";
import "@/assets/css/iconfont.css";
import "@/assets/scss/main.scss";
import "@/assets/css/main.css";

// =============== GLOBAL COMPONENTS
import VxTooltip from "@/layouts/components/VxTooltip";
import VxList from "@/components/VxList";
import VxBreadcrumb from "@/layouts/components/VxBreadcrumb";
import VxInputGroup from "@/components/VxInputGroup";
import FeatherIcon from "@/components/FeatherIcon";
import VxCard from "@/components/VxCard";
Vue.component(VxTooltip.name, VxTooltip);
Vue.component(VxCard.name, VxCard);
Vue.component(VxList.name, VxList);
Vue.component(VxBreadcrumb.name, VxBreadcrumb);
Vue.component(FeatherIcon.name, FeatherIcon);
Vue.component(VxInputGroup.name, VxInputGroup);

// =============== GLOBAL VARIABLES
Vue.prototype.$querystring = require("querystring");
Vue.prototype.$validator = new Validator();

// =============== CONFIGURATIONS
Vue.use(Vuesax, {
  theme: {
    colors: {
      primary: "#495678",
      success: "#28C76F",
      danger: "#EA5455",
      warning: "#FF9F43",
      dark: "#1E1E1E"
    }
  }
});

vSelect.props.components.default = () => ({
  Deselect: {
    render: createElement =>
      createElement("feather-icon", {
        props: {
          icon: "XIcon",
          svgClasses: "w-4 h-4 mt-1"
        }
      })
  },
  OpenIndicator: {
    render: createElement =>
      createElement("feather-icon", {
        props: {
          icon: "ChevronDownIcon",
          svgClasses: "w-5 h-5"
        }
      })
  }
});
Vue.component(vSelect);

Vue.use(VeeValidate);
Vue.use(VueHammer);

Vue.config.productionTip = false;

ApiService.init();
ApiService.setHeader();

router.beforeEach((to, from, next) =>
  Promise.all([store.dispatch(AUTH_IS_AUTHORIZED)]).then(next)
);

new Vue({
  router,
  store,
  acl,
  render: h => h(App)
}).$mount("#app");
