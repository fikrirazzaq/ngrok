// =============== LIBRARY
import ApiService, {
  AuthApiService,
  getErrorMessageDefault
} from "@/utils/common/apiService";
import AuthService from "@/utils/common/authService";
import { getRole, setRole, destroyRole } from "@/utils/common/aclService";
import { DEVELOP_URL } from "@/utils/common/config";
import {
  AUTH_UPDATE_ROLE,
  AUTH_LOGIN,
  AUTH_LOGOUT,
  AUTH_IS_AUTHORIZED,
  AUTH_ACTIVATE,
  AUTH_FORGOT_PASSWORD,
  AUTH_PASSWORD_SETTING
} from "../action-types";
import {
  AUTH_SET_USER_ROLE,
  AUTH_SET_ERROR,
  AUTH_SET_AUTH,
  AUTH_SET_ACTIVATION_URL,
  AUTH_SET_FORGOT_PASSWORD_URL,
  AUTH_PURGE,
  NTF_SET_UNREAD_COUNT,
  NTF_RESET,
  PROFILE_RESET,
  EMP_RESET,
  RPT_RESET
} from "../mutation-types";

// =============== STATE
const state = {
  user: AuthService.getUserProfile(),
  error: null,
  role: getRole() ? getRole() : "",
  isAuthenticated: AuthService.isAuthenticated(),
  isLoggedIn: AuthService.isUserLoggedIn(),
  opt: {
    activation_url: null,
    forgot_password_url: null
  }
};

// =============== GETTERS
const getters = {
  currentUser(state) {
    return state.user;
  },
  userDisplayName(state) {
    return state.user.fullname;
  },
  userDisplayImg(state) {
    return state.user.img;
  },
  userDisplayPic(state) {
    return state.user.picture;
  },
  userDisplayNip(state) {
    return state.user.nip;
  },
  userDisplayRole(state) {
    return state.role;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  },
  isLoggedIn(state) {
    return state.isLoggedIn;
  },
  activationUrl(state) {
    return state.opt.activation_url;
  }
};

// =============== ACTIONS
const actions = {
  [AUTH_UPDATE_ROLE](context, payload) {
    context.commit(AUTH_SET_USER_ROLE, payload);
  },
  [AUTH_ACTIVATE](context, payload) {
    return new Promise((resolve, reject) => {
      AuthApiService.activate({
        nip: payload.user_credential.nip,
        email: payload.user_credential.email
      })
        .then(({ data }) => {
          //
          // https://login.momentumdash.com/verify?email=rizkiyanapp%40gmail.com&token=aIAHTKfbyEGWQQ2WS1X1cQ
          // https://app.zeplin.io/verify?token=
          const url = `${DEVELOP_URL}login/password-setting/verify?token=${data.values.activation_token}&is_activation=true`;
          context.commit(AUTH_SET_ACTIVATION_URL, url);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: "Aktivasi Sudah Dikirim",
            text: "Silahkan periksa kotak masuk email anda.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          ApiService.setHeader();
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(AUTH_SET_ERROR, message.title);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [AUTH_FORGOT_PASSWORD](context, payload) {
    return new Promise((resolve, reject) => {
      AuthApiService.forgot({
        nip: "",
        email: payload.user_credential.email
      })
        .then(({ data }) => {
          //
          // https://login.momentumdash.com/verify?email=rizkiyanapp%40gmail.com&token=aIAHTKfbyEGWQQ2WS1X1cQ
          // https://app.zeplin.io/verify?token=
          const url = `${DEVELOP_URL}login/password-setting/verify?token=${data.values.activation_token}&is_activation=false`;
          context.commit(AUTH_SET_FORGOT_PASSWORD_URL, url);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: "Ubah Kata Sandi Sudah Dikirim",
            text: "Silahkan periksa kotak masuk email anda.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          ApiService.setHeader();
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(AUTH_SET_ERROR, message.title);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [AUTH_PASSWORD_SETTING](context, payload) {
    return new Promise((resolve, reject) => {
      AuthApiService.reset({
        token: payload.user_credential.token,
        password: payload.user_credential.password
      })
        .then(({ data }) => {
          payload.notify({
            time: 10000,
            title: "Kata Sandi Berhasil Diperbaharui.",
            text: "Silahkan masuk kemabli menggunakan kata sandi anda.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          ApiService.setHeader();
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(AUTH_SET_ERROR, message.title);
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [AUTH_LOGIN](context, payload) {
    if (state.isLoggedIn) {
      if (payload.closeLoading) payload.closeLoading();
      payload.notify({
        time: 10000,
        title: "Perhatian!",
        text: "Akun anda sudah login sebelumnya.",
        icon: "warning",
        color: "warning"
      });
      return false;
    } else {
      return new Promise((resolve, reject) => {
        AuthApiService.login({
          nip: payload.user_credential.nip,
          password: payload.user_credential.password
        })
          .then(({ data }) => {
            if (
              payload.login_type == true &&
              AuthService.getUserRole(data.values.role_id) == "admin"
            ) {
              context.commit(AUTH_SET_AUTH, data.values);
              if (payload.closeLoading) payload.closeLoading();
              payload.notify({
                time: 10000,
                title: "Login Berhasil",
                text: `Selamat datang kembali, Admin ${data.values.name}.`,
                icon: "check_circle",
                color: "success",
                position: "top-center"
              });
              ApiService.setHeader();
            } else if (
              payload.login_type == true &&
              AuthService.getUserRole(data.values.role_id) != "admin"
            ) {
              context.commit(AUTH_PURGE);
              context.commit(NTF_RESET);
              context.commit(PROFILE_RESET);
              context.commit(EMP_RESET);
              context.commit(RPT_RESET);
              if (payload.closeLoading) payload.closeLoading();
              payload.notify({
                time: 10000,
                title: "Gagal Login",
                text:
                  "Akun anda tidak dapat masuk sebagai admin, silahkan login kembali.",
                icon: "error",
                color: "danger",
                position: "top-center"
              });
              ApiService.setHeader();
            } else {
              data.values.role_id = 3;
              context.commit(AUTH_SET_AUTH, data.values);
              if (payload.closeLoading) payload.closeLoading();
              payload.notify({
                time: 10000,
                title: "Login Berhasil",
                text: `Selamat datang kembali, ${data.values.name}.`,
                icon: "check_circle",
                color: "success",
                position: "top-center"
              });
              ApiService.setHeader();
            }
            resolve(data);
          })
          .catch(({ response }) => {
            let message = getErrorMessageDefault();
            context.commit(AUTH_SET_ERROR, message.title);
            if (payload.closeLoading) payload.closeLoading();
            payload.notify({
              time: 10000,
              title: message.title,
              text: message.content,
              icon: "error",
              color: "danger",
              position: "top-center"
            });
            reject(response);
          });
      });
    }
  },
  [AUTH_LOGOUT](context, payload) {
    return new Promise((resolve, reject) => {
      AuthApiService.logout()
        .then(({ data }) => {
          context.commit(AUTH_PURGE);
          context.commit(NTF_RESET);
          context.commit(PROFILE_RESET);
          context.commit(EMP_RESET);
          context.commit(RPT_RESET);
          if (payload.closeLoading) payload.closeLoading();
          if (!!payload.notify) {
            payload.notify({
              time: 10000,
              title: "Logout Berhasil",
              text: "Silahkan login kembali untuk menggunakan akun anda.",
              icon: "check_circle",
              color: "success",
              position: "top-center"
            });
          }
          ApiService.setHeader();
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(AUTH_SET_ERROR, message.title);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [AUTH_IS_AUTHORIZED](context) {
    if (!!AuthService.getToken() && AuthService.isUserLoggedIn()) {
      AuthApiService.get(state.role)
        .then(({ data }) => {
          context.commit(NTF_SET_UNREAD_COUNT, data.values.notif_unread);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          if (!response) {
            response = {
              message: "Terjadi kesalahan saat pengecekan token."
            };
          }
          context.commit(AUTH_SET_ERROR, response.message);
          context.commit(AUTH_PURGE);
          context.commit(NTF_RESET);
          context.commit(PROFILE_RESET);
          context.commit(EMP_RESET);
          context.commit(RPT_RESET);
          ApiService.setHeader();
        });
    } else {
      context.commit(AUTH_PURGE);
      context.commit(NTF_RESET);
      context.commit(PROFILE_RESET);
      context.commit(EMP_RESET);
      context.commit(RPT_RESET);
      ApiService.setHeader();
    }
  }
};

// =============== MUTATIONS
const mutations = {
  [AUTH_SET_USER_ROLE](state, role) {
    state.role = role;
    setRole(role);
  },
  [AUTH_SET_ACTIVATION_URL](state, url) {
    state.opt.activation_url = url;
  },
  [AUTH_SET_FORGOT_PASSWORD_URL](state, url) {
    state.opt.forgot_password_url = url;
  },
  [AUTH_SET_ERROR](state, error) {
    state.error = error;
  },
  [AUTH_SET_AUTH](state, credentials) {
    const user = {
      email: credentials.email,
      fullname: credentials.name,
      nip: credentials.nip
    };

    user.img = "user.png";

    state.user = user;
    state.role = AuthService.getUserRole(credentials.role_id);
    state.isAuthenticated = true;
    state.isLoggedIn = true;
    state.error = null;
    AuthService.setUserProfile(user);
    AuthService.setToken(credentials.token);
    AuthService.setTokenExpiry(credentials.expires_at);
    setRole(AuthService.getUserRole(credentials.role_id));
  },
  [AUTH_PURGE](state) {
    state.user = AuthService.getUserProfile();
    state.role = "";
    state.isAuthenticated = false;
    state.isLoggedIn = false;
    state.error = null;
    AuthService.destroyToken();
    AuthService.destroyUserProfile();
    destroyRole();
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
