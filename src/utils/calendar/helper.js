// Minggu   => 0
// Senin    => 1
// Selasa   => 2
// Rabu     => 3
// Kamis    => 4
// Jumat    => 5
// Sabtu    => 6

function build(data) {
  let values = [];
  let date_key = new Date(Date.now());

  date_key.setDate(date_key.getDate() - (date_key.getDay() - 6));

  for (let index = 5; index > 0; index--) {
    let date_temp = new Date(JSON.parse(JSON.stringify(date_key)));
    date_temp.setDate(date_temp.getDate() - index);
    values.push({
      date: date_temp.toDateString(),
      day: date_temp.getDay(),
      events: []
    });
  }

  values.push({
    date: date_key.toDateString(),
    day: date_key.getDay(),
    events: []
  });

  date_key.setDate(date_key.getDate() + 1);
  values.push({
    date: date_key.toDateString(),
    day: date_key.getDay(),
    events: []
  });

  values.forEach(item => {
    item.events = data.filter(event => event.day == item.day);
  });

  return values;
}

function getActiveCalendar(calendar) {
  const now = new Date(Date.now());
  let res = [true, true, true, true, true, true, true];

  for (let index = 0; index < calendar.length; index++) {
    if (calendar[index].day < now.getDay()) {
      if (calendar[index].day != 0) {
        res[index] = false;
      }
    }
  }

  return res;
}

export default {
  build,
  getActiveCalendar
};
