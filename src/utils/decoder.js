export const blood = [
  { text: "O", value: 1 },
  { text: "A", value: 2 },
  { text: "B", value: 3 },
  { text: "AB", value: 4 }
];

export const degree = [
  { text: "S3", value: 1 },
  { text: "S2", value: 2 },
  { text: "S1", value: 3 },
  { text: "D4", value: 4 },
  { text: "D3", value: 5 },
  { text: "D2", value: 6 },
  { text: "D1", value: 7 }
];

export const gender = [
  { text: "Laki - laki", value: 1 },
  { text: "Perempuan", value: 2 }
];

export const gender2 = [
  { text: "Laki - laki", value: "L" },
  { text: "Perempuan", value: "P" }
];

export const gender3 = [{ text: "L", value: 1 }, { text: "P", value: 2 }];

export const golongan = [
  { text: "I/A - Juru Muda", value: 1 },
  { text: "I/B - Juru Muda Tingkat I", value: 2 },
  { text: "I/C - Juru", value: 3 },
  { text: "I/D - Juru Tingkat I", value: 4 },
  { text: "II/A - Pengatur Muda", value: 5 },
  { text: "II/B - Pengatur Muda Tingkat I", value: 6 },
  { text: "II/C - Pengatur", value: 7 },
  { text: "II/D - Pengatur Tingkat I", value: 8 },
  { text: "III/A - Penata Muda", value: 9 },
  { text: "III/B - Penata Muda Tingkat I", value: 10 },
  { text: "III/C - Penata", value: 11 },
  { text: "III/D - Penata Tingkat I", value: 12 },
  { text: "IV/A - Pembina", value: 13 },
  { text: "IV/B - Pembina Tingkat I", value: 14 },
  { text: "IV/C - Pembina Utama Muda", value: 15 },
  { text: "IV/D - Pembina Utama Madya", value: 16 },
  { text: "IV/E - Pembina Utama", value: 17 }
];

export const marital = [
  { text: "Kawin", value: 1 },
  { text: "Belum Kawin", value: 2 }
];

export const religion = [
  { text: "Islam", value: 1 },
  { text: "Kristen", value: 2 },
  { text: "Katolik", value: 3 },
  { text: "Hindu", value: 4 },
  { text: "Buddha", value: 5 },
  { text: "Kong Hu Cu", value: 6 }
];

export const unit = [
  { text: "Tata Usaha", value: 1, subunit: [] },
  {
    text: "Subdit Pemberdayaan Organisasi Pekerja & Organisasi Pengusaha",
    value: 2,
    subunit: [
      { text: "Seksi Organisasi Pengusaha", value: 21 },
      { text: "Seksi Organisasi Pekerja", value: 22 }
    ]
  },
  {
    text: "Subdit Kerjasama & Pemasyarakatan Hubungan Industrial",
    value: 3,
    subunit: [
      { text: "Seksi Kerjasama Hubungan Industrial", value: 31 },
      { text: "Seksi Pemasyarakatan Hubungan Industrial", value: 32 }
    ]
  },
  {
    text: "Subdit Kelembagaan Hubungan Industrial",
    value: 4,
    subunit: [
      { text: "Seksi Lembaga Kerjasama Bipartit", value: 41 },
      { text: "Seksi Lembaga Kerjasama Tripartit", value: 42 }
    ]
  }
];

export const position = [
  { text: "Struktural", value: 1 },
  { text: "Fungsional", value: 2 }
];

export default {
  blood,
  degree,
  gender,
  gender2,
  gender3,
  golongan,
  marital,
  religion,
  unit,
  position
};
