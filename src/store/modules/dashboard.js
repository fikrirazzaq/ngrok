// =============== LIBRARY
import {
  EmployeeBoardApiService,
  CalendarApiService,
  InformationBoardApiService,
  getErrorMessageDefault
} from "@/utils/common/apiService";
import {
  BRD_FETCH_CALENDAR,
  BRD_FETCH_EMPLOYEE,
  BRD_FETCH_INFORMATION,
  BRD_ADD_INFORMATION,
  BRD_DELETE_INFORMATION,
  BRD_UPDATE_INFORMATION
} from "../action-types";
import {
  BRD_SET_CALENDAR,
  BRD_SET_ACTIVE_CALENDAR,
  BRD_SET_EMPLOYEE,
  BRD_SET_INFORMATION,
  BRD_SET_ERROR,
  BRD_RESET
} from "../mutation-types";
import calendar_helper from "@/utils/calendar/helper";
import youtube_helper from "@/utils/youtube/helper";

// =============== STATE
const state = {
  calendar: null,
  activeList: null,
  promotionsBoard: null,
  birthdayBoard: null,
  retirementBoard: null,
  runningText: null,
  youtubeURL: null,
  news: null,
  error: null
};

// =============== GETTERS
const getters = {
  calendar(state) {
    return state.calendar;
  },
  activeList(state) {
    return state.activeList;
  },
  promotionsBoard(state) {
    return state.promotionsBoard;
  },
  birthdayBoard(state) {
    return state.birthdayBoard;
  },
  retirementBoard(state) {
    return state.retirementBoard;
  },
  runningText(state) {
    return state.runningText;
  },
  youtubeURL(state) {
    return state.youtubeURL;
  },
  news(state) {
    return state.news;
  }
};

// =============== ACTIONS
const actions = {
  [BRD_FETCH_CALENDAR](context, payload) {
    return new Promise((resolve, reject) => {
      CalendarApiService.get(payload.attempt)
        .then(({ data }) => {
          data.values = calendar_helper.build(data.values);
          let res = calendar_helper.getActiveCalendar(data.values);
          context.commit(BRD_SET_CALENDAR, {
            calendar: data.values,
            activeCalendar: res
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(BRD_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [BRD_FETCH_EMPLOYEE](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeBoardApiService.get(payload.type)
        .then(({ data }) => {
          context.commit(BRD_SET_EMPLOYEE, {
            type: payload.type,
            board: data.values
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(BRD_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [BRD_FETCH_INFORMATION](context, payload) {
    return new Promise((resolve, reject) => {
      InformationBoardApiService.get(payload.type)
        .then(({ data }) => {
          context.commit(BRD_SET_INFORMATION, {
            type: payload.type,
            content: data.values
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(BRD_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [BRD_ADD_INFORMATION](context, payload) {
    return new Promise((resolve, reject) => {
      InformationBoardApiService.add(payload.type, payload.information)
        .then(({ data }) => {
          payload.notify({
            time: 5000,
            title: "Berhasil.",
            text: "Data berhasil disimpan.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(BRD_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [BRD_DELETE_INFORMATION](context, payload) {
    return new Promise((resolve, reject) => {
      InformationBoardApiService.delete(payload.type, payload.information)
        .then(({ data }) => {
          payload.notify({
            time: 5000,
            title: "Berhasil.",
            text: "Data berhasil dihapus.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(BRD_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [BRD_UPDATE_INFORMATION](context, payload) {
    return new Promise((resolve, reject) => {
      InformationBoardApiService.edit(payload.type, payload.information)
        .then(({ data }) => {
          payload.notify({
            time: 5000,
            title: "Berhasil.",
            text: "Data berhasil disimpan.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(BRD_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  }
};

// =============== MUTATIONS
const mutations = {
  [BRD_SET_CALENDAR](state, payload) {
    state.calendar = payload.calendar;
    state.activeList = payload.activeCalendar;
  },
  [BRD_SET_ACTIVE_CALENDAR](state, payload) {
    state.activeList[payload.index] = payload.value;
  },
  [BRD_SET_EMPLOYEE](state, payload) {
    if (payload.type == "promotion") {
      state.promotionsBoard = payload.board;
    } else if (payload.type == "birthday") {
      state.birthdayBoard = payload.board;
    } else if (payload.type == "retire") {
      state.retirementBoard = payload.board;
    }
  },
  [BRD_SET_INFORMATION](state, payload) {
    if (payload.type == "running_text") {
      state.runningText = payload.content;
    } else if (payload.type == "youtube") {
      state.youtubeURL = {
        url: payload.content.url,
        id: youtube_helper.getPlaylistIdFromUrl(payload.content.url)
        // id: youtube_helper.getIdFromURL(payload.content.url)
      };
    } else if (payload.type == "news") {
      state.news = payload.content;
    }
  },
  [BRD_SET_ERROR](state, error) {
    state.error = error;
  },
  [BRD_RESET](state) {
    state.calendar = null;
    state.promotionsBoard = null;
    state.birthdayBoard = null;
    state.retirementBoard = null;
    state.runningText = null;
    state.youtubeURL = null;
    state.news = null;
    state.error = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
