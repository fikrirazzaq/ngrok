// =============== LIBRARY
import {
  ProfileApiService,
  getErrorMessageDefault
} from "@/utils/common/apiService";
import { PROFILE_FETCH, PROFILE_UPLOAD, PROFILE_DOCS } from "../action-types";
import {
  PROFILE_SET,
  PROFILE_SET_DOCS,
  PROFILE_SET_ERROR,
  PROFILE_RESET
} from "../mutation-types";

// =============== STATE
const state = {
  profile: null,
  profileDocs: null,
  error: null
};

// =============== GETTERS
const getters = {
  profile(state) {
    return state.profile;
  },
  profileDocs(state) {
    return state.profileDocs;
  }
};

// =============== ACTIONS
const actions = {
  [PROFILE_FETCH](context, payload) {
    return new Promise((resolve, reject) => {
      ProfileApiService.get()
        .then(({ data }) => {
          context.commit(PROFILE_SET, data.values);
          if (payload.closeLoading) payload.closeLoading();
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(PROFILE_SET_ERROR, message.title);
          if (payload.closeLoading) payload.closeLoading();
          payload.notify({
            time: 10000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [PROFILE_UPLOAD](context, payload) {
    return new Promise((resolve, reject) => {
      ProfileApiService.upload(payload)
        .then(({ data }) => {
          resolve(data);
        })
        .catch(({ response }) => {
          reject(response);
        });
    });
  },
  [PROFILE_DOCS](context, payload) {}
};

// =============== MUTATIONS
const mutations = {
  [PROFILE_SET](state, profile) {
    state.profile = profile;
  },
  [PROFILE_SET_DOCS](state, profileDocs) {
    state.profileDocs = profileDocs;
  },
  [PROFILE_SET_ERROR](state, error) {
    state.error = error;
  },
  [PROFILE_RESET](state) {
    state.profile = null;
    state.error = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
