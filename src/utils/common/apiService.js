// =============== LIBRARY
import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import AuthService from "@/utils/common/authService";
import { API_URL } from "@/utils/common/config";

// =============== SERVICES
const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
    Vue.axios.defaults.timeout = 5000;
  },

  setHeader() {
    Vue.axios.defaults.headers.post["Content-Type"] =
      "application/x-www-form-urlencoded";
    if (AuthService.isUserLoggedIn()) {
      Vue.axios.defaults.headers.common[
        "Authorization"
      ] = AuthService.getToken();
    }
  },

  get(resource) {
    return Vue.axios.get(`${resource}`).catch(error => {
      throw new Error(`[Kemnaker-Dashboard] ApiService ${error}`);
    });
  },

  post(resource, payload) {
    return Vue.axios.post(`${resource}`, payload).catch(error => {
      throw new Error(`[Kemnaker-Dashboard] ApiService ${error}`);
    });
  },

  put(resource, payload) {
    return Vue.axios.put(`${resource}`, payload).catch(error => {
      throw new Error(`[Kemnaker-Dashboard] ApiService ${error}`);
    });
  },

  delete(resource, payload) {
    return Vue.axios.delete(`${resource}`, payload).catch(error => {
      throw new Error(`[Kemnaker-Dashboard] ApiService ${error}`);
    });
  },

  query(resource, payload) {
    return Vue.axios.get(resource, { params: payload }).catch(error => {
      throw new Error(`[Kemnaker-Dashboard] ApiService ${error}`);
    });
  }
};

export default ApiService;

export const AuthApiService = {
  login(payload) {
    return ApiService.post("api/auth/login", payload);
  },
  logout(payload = {}) {
    return ApiService.put("api/auth/logout", payload);
  },
  activate(payload) {
    return ApiService.put("api/auth/activation", payload);
  },
  forgot(payload) {
    return ApiService.put("api/auth/activation", payload);
  },
  reset(payload) {
    return ApiService.put("api/auth/create_password", payload);
  },
  get(role) {
    return ApiService.get(`api/auth/${role}/data`);
  }
};

export const NotificationApiService = {
  get(role) {
    return ApiService.get(`api/notif/${role}`);
  },
  read(role, payload) {
    if (role == "admin") {
      return ApiService.post(`api/notif/${role}/read`, payload);
    } else if (role == "employee") {
      return ApiService.put(`api/notif/${role}/read`, payload);
    }
  }
};

export const ProfileApiService = {
  get() {
    return ApiService.get("api/profile/get");
  },
  upload(payload) {
    return ApiService.put("api/profile/files", payload);
  }
};

export const EmployeeApiService = {
  getall() {
    return ApiService.get("api/employees");
  },
  get(payload) {
    return ApiService.query("api/employee/nip", payload);
  },
  add(payload) {
    return ApiService.post("api/employee/add", payload);
  },
  edit(payload) {
    return ApiService.post("api/employee/update", payload);
  },
  delete(payload) {
    return ApiService.delete("api/employee/delete", payload);
  }
};

export const PromotionApiService = {
  send() {},
  validate() {}
};

export const RetirementApiService = {
  send() {},
  validate() {}
};

export const ReportApiService = {
  get() {
    return ApiService.get("api/employees");
  }
};

export const EmployeeBoardApiService = {
  get(type) {
    return ApiService.get(`api/dashboard/${type}/get`);
  }
};

export const CalendarApiService = {
  get(loop) {
    let service = ApiService.get("api/calendar/get");
    for (let index = 0; index < loop; index++) {
      service = ApiService.get("api/calendar/get");
    }
    return service;
  }
};

export const InformationBoardApiService = {
  get(type) {
    return ApiService.get(`api/board/${type}/get`);
  },
  add(type, payload) {
    return ApiService.post(`api/board/${type}/add`, payload);
  },
  edit(type, payload) {
    return ApiService.put(`api/board/${type}/update`, payload);
  },
  delete(type, payload) {
    return ApiService.delete(`api/board/${type}/delete`, payload);
  }
};

export const getErrorMessageDefault = () => {
  return {
    title: "Terjadi Kesalahan.",
    content: "Silahkan coba beberapa saat lagi."
  };
};
