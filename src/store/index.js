// =============== LIBRARY
import Vue from "vue";
import Vuex from "vuex";

import ui from "./modules/ui";
import auth from "./modules/auth";
import employee from "./modules/employee";
import profile from "./modules/profile";
import notification from "./modules/notification";
import promotion from "./modules/promotion";
import retirement from "./modules/retirement";
import report from "./modules/report";
import dashboard from "./modules/dashboard";

// =============== CONFIGURATIONS
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    ui,
    auth,
    employee,
    profile,
    notification,
    promotion,
    retirement,
    report,
    dashboard
  },
  strict: process.env.NODE_ENV !== "production"
});
