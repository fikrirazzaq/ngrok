// =============== LIBRARY
import {
  EmployeeApiService,
  ProfileApiService,
  getErrorMessageDefault
} from "@/utils/common/apiService";
import {
  EMP_FETCH,
  EMP_FETCH_ALL,
  EMP_ADD,
  EMP_EDIT,
  EMP_DELETE,
  EMP_UPLOAD,
  EMP_DOCS
} from "../action-types";
import {
  EMP_SET_EMPLOYEES,
  EMP_SET_SELECTED,
  EMP_SET_SELECTED_SINGLE,
  EMP_SET_DOCS,
  EMP_SET_ERROR,
  EMP_RESET
} from "../mutation-types";
import decoder from "../../utils/decoder";

// =============== STATE
const state = {
  employees: null,
  selectedEmployee: null,
  employeeDocs: null,
  error: null
};

// =============== GETTERS
const getters = {
  employees(state) {
    return state.employees;
  },
  selectedEmployee(state) {
    return state.selectedEmployee;
  },
  employeeDocs(state) {
    return state.employeeDocs;
  }
};

// =============== ACTIONS
const actions = {
  [EMP_FETCH_ALL](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.getall()
        .then(({ data }) => {
          context.commit(EMP_SET_EMPLOYEES, data.values);
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(EMP_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_FETCH](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.get({
        nip: payload.employee.nip
      })
        .then(({ data }) => {
          if (data.values.gender != "") {
            data.values.gender = decoder.gender3.filter(
              item => item.text == data.values.gender
            )[0].value;
          } else {
            data.values.gender = 1;
          }
          if (data.values.blood_type != "") {
            data.values.blood_type = decoder.blood.filter(
              item => item.text == data.values.blood_type
            )[0].value;
          } else {
            data.values.blood_type = 1;
          }
          if (data.values.religion != "") {
            data.values.religion = decoder.religion.filter(
              item => item.text == data.values.religion
            )[0].value;
          } else {
            data.values.religion = 1;
          }
          if (data.values.marital != "") {
            data.values.marital = decoder.marital.filter(
              item => item.text == data.values.marital
            )[0].value;
          } else {
            data.value.marital = 1;
          }
          if (data.values.position_type != "") {
            data.values.position_type = decoder.position.filter(
              item => item.text == data.values.position_type
            )[0].value;
          } else {
            data.values.position_type = 1;
          }
          if (data.values.unit != "") {
            data.values.unit = decoder.unit.filter(
              item => item.text == data.values.unit
            )[0].value;
          } else {
            data.values.unit = 1;
          }
          if (data.values.sub_unit != "") {
            data.values.sub_unit = decoder.unit
              .filter(item => item.value == data.values.unit)[0]
              .subunit.filter(
                item2 => item2.text == data.values.sub_unit
              )[0].value;
          }
          // data.values.golongan.ruang = decoder.golongan.filter(item =>
          //   item.text.includes(data.values.golongan.ruang)
          // )[0].value;
          for (let index = 0; index < data.values.education.length; index++) {
            data.values.education[index].degree = decoder.degree.filter(
              item => item.text == data.values.education[index].degree
            )[0].value;
          }

          context.commit(EMP_SET_SELECTED, data.values);
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(EMP_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_ADD](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.add(payload.user_form)
        .then(({ data }) => {
          payload.notify({
            time: 5000,
            title: "Data Berhasil Disimpan.",
            text:
              "Data " + payload.user_form.first_name + " berhasil disimpan.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(EMP_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_EDIT](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.edit(payload.user_form)
        .then(({ data }) => {
          payload.notify({
            time: 5000,
            title: "Data Berhasil Disimpan.",
            text:
              "Data " + payload.user_form.first_name + " berhasil disimpan.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          console.log(response);
          context.commit(EMP_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_DELETE](context, payload) {
    return new Promise((resolve, reject) => {
      EmployeeApiService.delete({
        nip: payload.employee.nip
      })
        .then(({ data }) => {
          payload.notify({
            time: 5000,
            title: "Berhasil.",
            text: "Data " + payload.employee.name + " berhasil dihapus.",
            icon: "check_circle",
            color: "success",
            position: "top-center"
          });
          resolve(data);
        })
        .catch(({ response }) => {
          let message = getErrorMessageDefault();
          context.commit(EMP_SET_ERROR, message.title);
          payload.notify({
            time: 5000,
            title: message.title,
            text: message.content,
            icon: "error",
            color: "danger",
            position: "top-center"
          });
          reject(response);
        });
    });
  },
  [EMP_UPLOAD](context, payload) {
    return new Promise((resolve, reject) => {
      ProfileApiService.upload(payload)
        .then(({ data }) => {
          resolve(data);
        })
        .catch(({ response }) => {
          reject(response);
        });
    });
  },
  [EMP_DOCS](context, payload) {}
};

// =============== MUTATIONS
const mutations = {
  [EMP_SET_EMPLOYEES](state, employees) {
    state.employees = employees;
  },
  [EMP_SET_SELECTED](state, employee) {
    state.selectedEmployee = employee;
  },
  [EMP_SET_SELECTED_SINGLE](state, payload) {
    state.selectedEmployee[payload.type] = payload.value;
  },
  [EMP_SET_DOCS](state, employeeDocs) {
    state.employeeDocs = employeeDocs;
  },
  [EMP_SET_ERROR](state, error) {
    state.error = error;
  },
  [EMP_RESET](state) {
    state.employees = null;
    state.selectedEmployee = null;
    state.error = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
