// =============== LIBRARY

// =============== STATE
const state = {};

// =============== GETTERS
const getters = {};

// =============== ACTIONS
const actions = {};

// =============== MUTATIONS
const mutations = {};

export default {
  state,
  getters,
  actions,
  mutations
};
