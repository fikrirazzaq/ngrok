// =============== LIBRARY
import { ReportApiService } from "@/utils/common/apiService";
import { RPT_GENERATE } from "../action-types";
import {
  RPT_SET_COLUMNS,
  RPT_SET_ROWS,
  RPT_SET_ERROR,
  RPT_RESET
} from "../mutation-types";

// =============== STATE
const state = {
  columns: null,
  rows: null,
  error: null
};

// =============== GETTERS
const getters = {
  columns(state) {
    return state.columns;
  },
  rows(state) {
    return state.rows;
  }
};

// =============== ACTIONS
const actions = {
  [RPT_GENERATE](context, payload) {}
};

// =============== MUTATIONS
const mutations = {
  [RPT_SET_COLUMNS](state, columns) {
    state.columns = columns;
  },
  [RPT_SET_ROWS](state, rows) {
    state.rows = rows;
  },
  [RPT_SET_ERROR](state, error) {
    state.error = error;
  },
  [RPT_RESET](state) {
    state.columns = null;
    state.rows = null;
    state.error = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
