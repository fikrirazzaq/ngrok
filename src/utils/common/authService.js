const ID_TOKEN_KEY = "private_token";
const ID_TOKEN_EXPIRY = "private_token_expiry";
const USER_PROFILE_KEY = "user_account";

export const getDefaultUserProfile = () => {
  return {
    email: "",
    fullname: "",
    nip: "",
    img: "user.png"
  };
};

export const getToken = () => {
  return localStorage.getItem(ID_TOKEN_KEY);
};

export const setToken = token => {
  localStorage.setItem(ID_TOKEN_KEY, token);
};

export const getTokenExpiry = () => {
  return localStorage.getItem(ID_TOKEN_EXPIRY);
};

export const setTokenExpiry = expiry => {
  localStorage.setItem(ID_TOKEN_EXPIRY, expiry);
};

export const destroyToken = () => {
  localStorage.removeItem(ID_TOKEN_KEY);
  localStorage.removeItem(ID_TOKEN_EXPIRY);
};

export const getUserProfile = () => {
  if (localStorage.getItem(USER_PROFILE_KEY)) {
    return JSON.parse(localStorage.getItem(USER_PROFILE_KEY));
  } else {
    return getDefaultUserProfile();
  }
};

export const setUserProfile = user => {
  localStorage.setItem(USER_PROFILE_KEY, JSON.stringify(user));
};

export const destroyUserProfile = () => {
  localStorage.removeItem(USER_PROFILE_KEY);
};

export const isAuthenticated = () => {
  return new Date(Date.now()) < new Date(getTokenExpiry());
};

export const isUserLoggedIn = () => {
  return (
    !!JSON.parse(localStorage.getItem(USER_PROFILE_KEY)) && isAuthenticated()
  );
};

export const getUserRole = roleId => {
  if (roleId === 1) {
    return "superadmin";
  } else if (roleId === 2) {
    return "admin";
  } else if (roleId === 3) {
    return "employee";
  } else {
    return "public";
  }
};

export default {
  getDefaultUserProfile,
  getToken,
  setToken,
  getTokenExpiry,
  setTokenExpiry,
  destroyToken,
  getUserProfile,
  setUserProfile,
  destroyUserProfile,
  isAuthenticated,
  isUserLoggedIn,
  getUserRole
};
